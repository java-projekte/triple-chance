package com.bufkin.triple_chance;

import java.util.Random;
import java.util.Scanner;

public class Main {
	public static void main(String[] args) {
		char ansFormat;
		try (Scanner userInput = new Scanner(System.in)) {
			Random rand = new Random();

			boolean isNumber;
			char ans;
			double prize = 0.0;
			double priceTotal = 0;
			double gambleMoney = 0;

			do {
				System.out.println("=======================================");
				// VALIDATION FOR USER INPUT
				System.out.print("Enter $ to gamble: ");
				if (userInput.hasNextDouble()) {
					gambleMoney = userInput.nextDouble();

					if (gambleMoney <= 0) {
						userInput.nextLine();
						throw new NegativeArraySizeException("Must be a positive number.");
					} else {
						break;
					}
				} else {
					userInput.nextLine();
					throw new NegativeArraySizeException("Must be a positive number.");
				}
			} while (!(isNumber));
			// RANDOMIZED RESULTS
			int imgIdx1 = rand.nextInt(6);
			int imgIdx2 = rand.nextInt(6);
			int imgIdx3 = rand.nextInt(6);
			String[] imgMatch = {"Cherries", "Oranges", "Plums", "Bells", "Melons", "Bars"};
			System.out.print("\nResults: ");
			System.out.println(imgMatch[imgIdx1] + "   " + imgMatch[imgIdx2] + "   " + imgMatch[imgIdx3] + "   ");

			// RESULTS MATCHING + PRIZE
			if (!imgMatch[imgIdx1].equals(imgMatch[imgIdx2]) || !imgMatch[imgIdx1].equals(imgMatch[imgIdx3])) {
				if (imgMatch[imgIdx1].equals(imgMatch[imgIdx2]) || imgMatch[imgIdx1].equals(imgMatch[imgIdx3])
						|| imgMatch[imgIdx2].equals(imgMatch[imgIdx3])) {
					System.out.println("You won double!");
					prize = 2 * gambleMoney;
					priceTotal += prize;
				} else {
					System.out.println("You lost!");
				}
			} else {
				System.out.println("You won triple!");
				prize = 3 * gambleMoney;
				priceTotal += prize;
			}

			System.out.println("Round Prize: " + prize);

			// REPEAT
			System.out.print("\nPlay again (y/n)? ");
			ans = userInput.next().charAt(0);
			ansFormat = Character.toLowerCase(ans);

			// REPEAT VALIDATION
			while (ansFormat != 'y' && ansFormat != 'n') {
				System.err.println("ERROR-2A: Must be y/n only.");
				System.out.print("Play again (y/n)? ");
				ans = userInput.next().charAt(0);
				ansFormat = Character.toLowerCase(ans);
			}
		}
		// while (ansFormat == 'y') ;
		System.out.println("Total prize: " + priceTotal);
	}
	}
}