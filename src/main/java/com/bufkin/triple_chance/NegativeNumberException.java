package main.java.com.bufkin.triple_chance;

import java.io.Serial;

public class NegativeNumberException extends Exception {
	@Serial
	private static final long serialVersionUID = 1L;

	public NegativeNumberException(String errorMessage) {
		super(errorMessage);
	}
}
