package main.java.com.bufkin.triple_chance;

import java.io.Serial;

public class WrongInputException extends Exception {
	@Serial
	private static final long serialVersionUID = 1L;

	public WrongInputException(String errorMessage) {
		super(errorMessage);
	}
}
